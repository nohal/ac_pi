/******************************************************************************
 *
 * Project:  OpenCPN
 * Purpose:  ActiveCaptain Plugin
 * Author:   Pavel Kalian
 *
 ***************************************************************************
 *   Copyright (C) 2014 by Pavel Kalian                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA.         *
 ***************************************************************************
 */

#include "ac_pi.h"
#include "ac_pi_gui_impl.h"
#include "ac_pi_util.h"

/** \brief Draws message on the screen
 *
 * \param msg wxString Message
 * \param x int X coordinate
 * \param y int Y coordinate
 * \param mfont wxFont* Font
 * \return void
 *
 */       
void ac_pi::DrawMessageWindow( wxString msg, int x, int y, wxFont *mfont )
{
    if(msg.empty())
        return;

    if(m_pdc) {
        wxDC &dc = *m_pdc;
        dc.SetFont( *mfont );
        dc.SetPen( *wxTRANSPARENT_PEN);

        dc.SetBrush( wxColour( 243, 229, 47 ) );
        int w, h;
        dc.GetMultiLineTextExtent( msg, &w, &h );
        h += 2;
        int yp = y - ( GetChartbarHeight() + h );

        int label_offset = 10;
        int wdraw = w + ( label_offset * 2 );
        dc.DrawRectangle( 0, yp, wdraw, h );
        dc.DrawLabel( msg, wxRect( label_offset, yp, wdraw, h ),
                      wxALIGN_LEFT | wxALIGN_CENTRE_VERTICAL);
    }
    else
    {
        m_TexFontMessage.Build(*mfont);
        int w, h;
        m_TexFontMessage.GetTextExtent( msg, &w, &h);
        h += 2;
        int yp = y - ( GetChartbarHeight() + h );

        glColor3ub( 243, 229, 47 );

        glBegin(GL_QUADS);
        glVertex2i(0, yp);
        glVertex2i(w, yp);
        glVertex2i(w, yp+h);
        glVertex2i(0, yp+h);
        glEnd();

        glEnable(GL_BLEND);
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

        glColor3ub( 0, 0, 0 );
        glEnable(GL_TEXTURE_2D);
        m_TexFontMessage.RenderString( msg, 0, yp);
        glDisable(GL_TEXTURE_2D);
    }
}

/** \brief Calculates the extents of a given text while rendered using a specified font
 *
 * \param msg wxString The text
 * \param mfont wxFont* The font to be used for rendering
 * \param w int* Return parameter. Width of the rendered text
 * \param h int* Return parameter. Height of the rendered text
 * \return void
 *
 */       
void ac_pi::GetTextExtent( wxString msg, wxFont *mfont, int *w, int *h )
{
    if( msg.empty() )
        return;

    if(m_pdc)
    {
        m_pdc->SetFont( *mfont );
        m_pdc->GetMultiLineTextExtent( msg, w, h );
    }
    else
    {
        TexFont TexFontMessage;
        TexFontMessage.Build(*mfont);
        TexFontMessage.GetTextExtent( msg, w, h );
    }
}

/** \brief Renders text on DC or OpenGL canvas
 *
 * \param msg wxString Text to be rendered
 * \param hotspot wxPoint Upper left coordinate to start rendering
 * \param mfont wxFont* Font to use for rendering
 * \return void
 *
 */      
void ac_pi::DrawText( wxString msg, wxPoint hotspot, wxFont *mfont )
{
    if(msg.empty())
        return;

    if(m_pdc)
    {
        m_pdc->SetFont( *mfont );
        m_pdc->SetPen( *wxTRANSPARENT_PEN);

        int w, h;
        m_pdc->GetMultiLineTextExtent( msg, &w, &h );

        m_pdc->DrawLabel( msg, wxRect( hotspot.x, hotspot.y, w, h ),
                      wxALIGN_LEFT | wxALIGN_CENTRE_VERTICAL);
    }
    else
    {
        TexFont TexFontMessage;
        TexFontMessage.Build(*mfont);

        glEnable(GL_BLEND);
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

        glColor3ub( 0, 0, 0 );
        glEnable(GL_TEXTURE_2D);
        TexFontMessage.RenderString( msg, hotspot.x, hotspot.y );
        glDisable(GL_TEXTURE_2D);
    }
}


/** \brief Draws an AC marker on the canvas DC or OpenGL. Used to display the marker without mouse focus.
 *
 * \param hotspot wxPoint Coordinates of the tip of the marker
 * \param marker AcMarker The marker to render
 * \return void
 *
 */     
void ac_pi::DrawMarker(wxPoint hotspot, AcMarker marker)
{
    int size = 3;
    wxPoint points[11] = {
        wxPoint(0 * size, 0 * size),
        wxPoint(-1 * size, -2 * size),
        wxPoint(-2 * size, -2 * size),
        wxPoint(-3 * size, -3 * size),
        wxPoint(-3 * size, -7 * size),
        wxPoint(-2 * size, -8 * size),
        wxPoint(2 * size, -8 * size),
        wxPoint(3 * size, -7 * size),
        wxPoint(3 * size, -3 * size),
        wxPoint(2 * size, -2 * size),
        wxPoint(1 * size, -2 * size)
    };
    
    wxImage badges( 100, 100 );
    int badge_width = 0;
    int badge_height = 0;
    if( marker.GetBadges() > 0 )
    {
        int cur_x = 0;
        if( marker.GetBadges() == 0x1 || (marker.GetType() == AC_LOCAL_KNOWLEDGE && marker.GetBadges() > 0x1) )
        {
            badge_width += _img_badge_star->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_star->GetHeight());
            badges.Paste( _img_badge_star->ConvertToImage(), cur_x, 0 );
        }
        else if( marker.GetBadges() == 0x3 )
        {
            badge_width += _img_badge_first->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_first->GetHeight());
            badges.Paste( _img_badge_first->ConvertToImage(), cur_x, 0 );
        }
        else if( marker.GetBadges() == 0x5 )
        {
            badge_width += _img_badge_coop->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_coop->GetHeight());
            badges.Paste( _img_badge_coop->ConvertToImage(), cur_x, 0 );
        }
        else if( marker.GetBadges() == 0x7 )
        {
            badge_width += _img_badge_first->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_first->GetHeight());
            badges.Paste( _img_badge_first->ConvertToImage(), cur_x, 0 );
            cur_x = badge_width;
            badge_width += _img_badge_coop->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_coop->GetHeight());
            badges.Paste( _img_badge_coop->ConvertToImage(), cur_x, 0 );
        }
        else if( marker.GetBadges() == 0x11 )
        {
            badge_width += _img_badge_wifi->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_wifi->GetHeight());
            badges.Paste( _img_badge_wifi->ConvertToImage(), cur_x, 0 );
            cur_x = badge_width;
            badge_width += _img_badge_star->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_star->GetHeight());
            badges.Paste( _img_badge_star->ConvertToImage(), cur_x, 0 );
        }
        else if( marker.GetBadges() == 0x13 )
        {
            badge_width += _img_badge_wifi->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_wifi->GetHeight());
            badges.Paste( _img_badge_wifi->ConvertToImage(), cur_x, 0 );
            cur_x = badge_width;
            badge_width += _img_badge_first->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_first->GetHeight());
            badges.Paste( _img_badge_first->ConvertToImage(), cur_x, 0 );
        }
        else if( marker.GetBadges() == 0x15 )
        {
            badge_width += _img_badge_wifi->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_wifi->GetHeight());
            badges.Paste( _img_badge_wifi->ConvertToImage(), cur_x, 0 );
            cur_x = badge_width;
            badge_width += _img_badge_coop->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_coop->GetHeight());
            badges.Paste( _img_badge_coop->ConvertToImage(), cur_x, 0 );
        }
        else if( marker.GetBadges() == 0x17 )
        {
            badge_width += _img_badge_wifi->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_wifi->GetHeight());
            badges.Paste( _img_badge_wifi->ConvertToImage(), cur_x, 0 );
            cur_x = badge_width;
            badge_width += _img_badge_coop->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_coop->GetHeight());
            badges.Paste( _img_badge_coop->ConvertToImage(), cur_x, 0 );
            cur_x = badge_width;
            badge_width += _img_badge_first->GetWidth();
            badge_height = wxMax(badge_height, _img_badge_first->GetHeight());
            badges.Paste( _img_badge_first->ConvertToImage(), cur_x, 0 );
        }
        badges.Resize( wxSize(badge_width, badge_height), wxPoint(0, 0) );
    }
    
    wxColour blk;
    wxBrush fil = wxBrush(marker.GetColor());
    GetGlobalColor(_T("UBLCK"), &blk);
    if( badge_width > 0 )
    {
        points[6].x += badge_width - 4 * size;
        points[7].x += badge_width - 4 * size;
        points[8].x += badge_width - 4 * size;
        points[9].x += badge_width - 4 * size;
    }
    
        wxPoint triangle[3] = {
        points[0],
        points[1],
        points[10]
    };
    wxPoint top[9] = {
        points[1],
        points[2],
        points[3],
        points[4],
        points[5],
        points[6],
        points[7],
        points[8],
        points[9]
    };
    
    if( m_pdc )
    {
        DrawPolygon( 11, points, hotspot.x, hotspot.y, wxPen( blk, wxMin(size - 1, 3) ), fil );
    }
    else
    {
        DrawPolygon( 3, triangle, hotspot.x, hotspot.y, wxNullPen, fil );
        DrawPolygon( 9, top, hotspot.x, hotspot.y, wxNullPen, fil );
        DrawPolygon( 11, points, hotspot.x, hotspot.y, wxPen( blk, wxMin(size - 1, 3) ), wxNullBrush );
    }
    if( marker.GetBadges() > 0 )
        DrawBitmap( wxBitmap(badges), hotspot.x - 2 * size, hotspot.y - 7 * size );
}


/** \brief Draws a bitmap on the canvas DC or OpenGL
 *
 * \param bmp wxBitmap& Bitmap to draw
 * \param x int X coordinate
 * \param y int Y coordinate
 * \return void
 *
 */      
void ac_pi::DrawBitmap( wxBitmap bmp, int x, int y )
{
    if( m_pdc )
    {
        m_pdc->DrawBitmap( bmp, x, y );
    }
    else
    {
        int glw, glh;
        unsigned int icon_texture = GetIconTexture( &bmp, glw, glh ); //TODO: This is a total waste and probably a leak of textures
        
        glBindTexture(GL_TEXTURE_2D, icon_texture);
        
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
        
        glColor3f(1, 1, 1);
        
        int w = bmp.GetWidth(), h = bmp.GetHeight();
        float u = (float)w/glw, v = (float)h/glh;
        glBegin(GL_QUADS);
        glTexCoord2f(0, 0); glVertex2f(x, y);
        glTexCoord2f(u, 0); glVertex2f(x+w, y);
        glTexCoord2f(u, v); glVertex2f(x+w, y+h);
        glTexCoord2f(0, v); glVertex2f(x, y+h);
        glEnd();
        glDisable(GL_BLEND);
        glDisable(GL_TEXTURE_2D);
    }
}

/** \brief Creates a texture for the bitmap image
 *
 * \param pbm const wxBitmap* Bitmap image
 * \param glw int& Width of the generated texture
 * \param glh int& Height of the generated texture
 * \return unsigned int ID of the generated texture
 *
 */      
unsigned int ac_pi::GetIconTexture( const wxBitmap *pbm, int &glw, int &glh )
{
    unsigned int icon_texture;
    /* make rgba texture */
    glGenTextures(1, &icon_texture);
    glBindTexture(GL_TEXTURE_2D, icon_texture);
            
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    
    wxImage image = pbm->ConvertToImage();
    int w = image.GetWidth(), h = image.GetHeight();
    
    glw = NextPow2(w);
    glh = NextPow2(h);
    
    unsigned char *d = image.GetData();
    unsigned char *a = image.GetAlpha();
        
    unsigned char mr, mg, mb;
    image.GetOrFindMaskColour( &mr, &mg, &mb );

    unsigned char *e = new unsigned char[4 * w * h];
    for( int y = 0; y < h; y++ )
        for( int x = 0; x < w; x++ ) {
            unsigned char r, g, b;
            int off = ( y * image.GetWidth() + x );
            r = d[off * 3 + 0];
            g = d[off * 3 + 1];
            b = d[off * 3 + 2];
            
            e[off * 4 + 0] = r;
            e[off * 4 + 1] = g;
            e[off * 4 + 2] = b;
            
            e[off * 4 + 3] =  a ? a[off] : ( ( r == mr ) && ( g == mg ) && ( b == mb ) ? 0 : 255 );
        }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, glw, glh,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h,
                    GL_RGBA, GL_UNSIGNED_BYTE, e);

    delete [] e;

    return icon_texture;
}


/** \brief Draws an AC marker with information on the canvas DC or OpenGL. Used to display the marker with mouse focus.
 *
 * \param hotspot wxPoint Coordinates of the tip of the marker
 * \param marker AcMarker The marker to render
 * \return void
 *
 */     
void ac_pi::DrawUnrolledMarker(wxPoint hotspot, AcMarker marker)
{
    int size = 3;
    int step = 12;
    int namew = 0;
    int nameh = 0;
    
    GetTextExtent( marker.GetName(), m_dFont_map, &namew, &nameh );
    namew = wxMax(namew, 100);
    
    wxPoint points[12] = {
        wxPoint(0 * size, 0 * size),
        wxPoint(-1 * size, -2 * size),
        wxPoint(-2 * size, -2 * size),
        wxPoint(-3 * size, -3 * size),
        wxPoint(-3 * size, -7 * size - nameh),
        wxPoint(-2 * size, -8 * size - nameh),
        wxPoint(2 * size + namew, -8 * size - nameh),
        wxPoint(3 * size + namew, -7 * size - nameh),
        wxPoint(3 * size + namew, -3 * size),
        wxPoint(2 * size + namew, -2 * size),
        wxPoint(1 * size, -2 * size),
    };
    
    wxPoint triangle[3] = {
        points[0],
        points[1],
        points[11]
    };
    wxPoint top[11] = {
        points[1],
        points[2],
        points[3],
        points[4],
        points[5],
        points[6],
        points[7],
        points[8],
        points[9],
        points[10]
    };
    
    wxColour wht;
    wxPen stroke = wxPen(marker.GetColor(), wxMin(size - 1, 3));
    GetGlobalColor(_T("UWHIT"), &wht);
    wxBrush fil = wxBrush(wht);
    if( m_pdc )
    {
        DrawPolygon( 11, points, hotspot.x, hotspot.y, stroke, fil );
    }
    else
    {
        DrawPolygon( 3, triangle, hotspot.x, hotspot.y, wxNullPen, fil );
        DrawPolygon( 11, top, hotspot.x, hotspot.y, wxNullPen, fil );
        DrawPolygon( 12, points, hotspot.x, hotspot.y, stroke, wxNullBrush );
    }
    
    DrawText( marker.GetName(), wxPoint(hotspot.x, hotspot.y - nameh - 20), m_dFont_map );
    if( marker.GetType() == AC_HAZARD )
    {
        wxColour blk;
        wxColour fil = marker.GetColor();
        GetGlobalColor(_T("UBLCK"), &blk);
        wxPen pen(blk, 1);
        wxBrush brush(fil);
        wxString txt = wxString::Format(_T("(%i)"), marker.GetCommentCount());
        int w, h;
        GetTextExtent(txt, m_dFont_map, &w, &h);
        
        DrawText( txt, wxPoint(hotspot.x + namew - w, hotspot.y - 22), m_dFont_map);
        DrawExclamation( wxPoint(hotspot.x + namew - w - step, hotspot.y - 20), pen, brush );
    }
    else
        DrawStars( wxPoint(hotspot.x + namew - marker.GetRatingStars() * step, hotspot.y - 20), marker );
}

/** \brief Draws one rating star on DC or OpenGL canvas
 *
 * \param hotspot wxPoint Coordinates of the upper left corner
 * \param pen wxPen Stroke pen
 * \param brush wxBrush Fill brush
 * \return void
 *
 */      
void ac_pi::DrawStar(wxPoint hotspot, wxPen pen, wxBrush brush)
{
    float size = 0.1;
    wxPoint points[11] = {
        wxPoint(0 * size, 0 * size),
        wxPoint(-15 * size, 33 * size),
        wxPoint(-50 * size, 38 * size),
        wxPoint(-25 * size, 64 * size),
        wxPoint(-31 * size, 100 * size),
        wxPoint(0 * size, 83 * size),
        wxPoint(31 * size, 100 * size),
        wxPoint(25 * size, 64 * size),
        wxPoint(50 * size, 38 * size),
        wxPoint(15 * size, 33 * size),
        wxPoint(0 * size, 0 * size)
    };
    
    wxPoint center[5] = {
        points[1],
        points[3],
        points[5],
        points[7],
        points[9]
    };
    
    wxPoint t1[3] = {
        points[0],
        points[1],
        points[9]
    };
    wxPoint t2[3] = {
        points[1],
        points[2],
        points[3]
    };
    wxPoint t3[3] = {
        points[3],
        points[4],
        points[5]
    };
    wxPoint t4[3] = {
        points[5],
        points[6],
        points[7]
    };
    wxPoint t5[3] = {
        points[7],
        points[8],
        points[9]
    };

    if( m_pdc )
    {
        DrawPolygon( 10, points, hotspot.x, hotspot.y, pen, brush );
    }
    else
    {
        DrawPolygon( 3, t1, hotspot.x, hotspot.y, wxNullPen, brush );
        DrawPolygon( 3, t2, hotspot.x, hotspot.y, wxNullPen, brush );
        DrawPolygon( 3, t3, hotspot.x, hotspot.y, wxNullPen, brush );
        DrawPolygon( 3, t4, hotspot.x, hotspot.y, wxNullPen, brush );
        DrawPolygon( 3, t5, hotspot.x, hotspot.y, wxNullPen, brush );
        DrawPolygon( 5, center, hotspot.x, hotspot.y, wxNullPen, brush );
        DrawPolygon( 10, points, hotspot.x, hotspot.y, pen, wxNullBrush );
    }
    
}

/** \brief Draws an exclamation on DC or OpenGL canvas
 *
 * \param hotspot wxPoint Coordinates of the upper left corner
 * \param pen wxPen Stroke pen
 * \param brush wxBrush Fill brush
 * \return void
 *
 */      
void ac_pi::DrawExclamation(wxPoint hotspot, wxPen pen, wxBrush brush)
{
    float size = 0.1;
    wxPoint points[3] = {
        wxPoint(0 * size, 0 * size),
        wxPoint(-50 * size, 100 * size),
        wxPoint(50 * size, 100 * size)
    };
    
    wxPoint top[4] = {
        wxPoint(-5 * size, 10 * size),
        wxPoint(5 * size, 10 * size),
        wxPoint(5 * size, 85 * size),
        wxPoint(-5 * size, 85 * size)
    };
    
    wxPoint dot[4] = {
        wxPoint(-5 * size, 90 * size),
        wxPoint(5 * size, 90 * size),
        wxPoint(5 * size, 95 * size),
        wxPoint(-5 * size, 95 * size)
    };
    
    DrawPolygon( 3, points, hotspot.x, hotspot.y, pen, brush );
    DrawPolygon( 4, top, hotspot.x, hotspot.y, pen, brush );
    DrawPolygon( 4, dot, hotspot.x, hotspot.y, pen, brush );
    
}

/** \brief Draws the complete block of the rating stars and number of ratings on DC or OpenGL canvas.
 *         Part of the unrolled marker rendering.
 *
 * \param hotspot wxPoint Coordinates of the upper left corner
 * \param marker AcMarker Marker being displayed
 * \return void
 *
 */     
void ac_pi::DrawStars(wxPoint hotspot, AcMarker marker)
{
    int step = 12;
    wxString txt = wxString::Format(_T("(%i)"), marker.GetRatingCount());
    int w, h;
    GetTextExtent(txt, m_dFont_map, &w, &h);
    int shift = w - 5;
    wxColour blk;
    wxColour fil = marker.GetColor();
    GetGlobalColor(_T("UBLCK"), &blk);
    wxPen pen(blk, 1);
    wxBrush brush(fil);
    
    for( int i = 0; i < marker.GetRatingStars(); i++ )
        DrawStar( wxPoint(hotspot.x + i * step - shift, hotspot.y), pen, brush );
    DrawText( txt, wxPoint(hotspot.x + marker.GetRatingStars() * step - 5 - shift, hotspot.y - 2), m_dFont_map);
}

/** \brief Sets the desired stroke pen for OpenGL canvas
 *
 * \param pen wxPen Pen to be used
 * \return bool True if the pen is not non-existent or transparent
 *
 */    
bool ac_pi::ConfigurePen(wxPen pen)
{
    if( pen == wxNullPen )
        return false;
    if( pen == *wxTRANSPARENT_PEN )
        return false;

#ifdef ocpnUSE_GL
    wxColour c = pen.GetColour();
    int width = pen.GetWidth();
    glColor4ub( c.Red(), c.Green(), c.Blue(), c.Alpha() );
    glLineWidth( width );
#endif    
    return true;
}

/** \brief Sets the desired fill brush for OpenGL canvas
 *
 * \param pen wxPen Pen to be used
 * \return bool True if the brush is not non-existent or transparent
 *
 */    
bool ac_pi::ConfigureBrush(wxBrush brush)
{
    if( brush == wxNullBrush || brush.GetStyle() == wxTRANSPARENT )
        return false;
#ifdef ocpnUSE_GL
    wxColour c = brush.GetColour();
    glColor4ub( c.Red(), c.Green(), c.Blue(), c.Alpha() );
#endif    
    return true;
}

#ifdef ocpnUSE_GL
/** \brief Draws a line of given width between 2 points on OpenGL canvas
 *
 * \param x1 double 1st point x-coordinate
 * \param y1 double 1st point y-coordinate
 * \param x2 double 2nd point x-coordinate
 * \param y2 double 2nd point y-coordinate
 * \param width double Width of the line
 * \return void
 *
 */        
void ac_pi::DrawGLLine( double x1, double y1, double x2, double y2, double width )
{
    {
        glPushAttrib(GL_COLOR_BUFFER_BIT | GL_LINE_BIT | GL_ENABLE_BIT |
                     GL_POLYGON_BIT | GL_HINT_BIT ); //Save state
        {

            //      Enable anti-aliased lines, at best quality
            glEnable( GL_LINE_SMOOTH );
            glEnable( GL_BLEND );
            glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
            glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
            glLineWidth( width );

            glBegin( GL_LINES );
            glVertex2d( x1, y1 );
            glVertex2d( x2, y2 );
            glEnd();
        }

        glPopAttrib();
    }
}
#endif

/** \brief Sets the basic OpenGL drawing parameters
 *
 * \param highQuality bool True if higher quality output is desired
 * \return void
 *
 */    
void ac_pi::SetGLAttrs( bool highQuality )
{
#ifdef ocpnUSE_GL
    
    //      Enable anti-aliased polys, at best quality
    if( highQuality ) {
        glEnable( GL_LINE_SMOOTH );
        glEnable( GL_POLYGON_SMOOTH );
        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
        glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
    } else {
        glDisable( GL_LINE_SMOOTH );
        glDisable( GL_POLYGON_SMOOTH );
        glDisable( GL_BLEND );
    }
#endif    
}

/** \brief Draws a filled polygon on DC or OpenGL canvas. On OpenGL only CONVEX polygons are supported correctly!
 *
 * \param n int Number of points in the polygon
 * \param points[] wxPoint Coordinates of the points of the polygon
 * \param xoffset wxCoord Center x-coordinate
 * \param yoffset wxCoord Center y-coordinate
 * \param pen wxPen Stroke pen to use
 * \param brush wxBrush Fill brush to use
 * \return void
 *
 */         
void ac_pi::DrawPolygon( int n, wxPoint points[], wxCoord xoffset, wxCoord yoffset, wxPen pen, wxBrush brush )
{
    if( m_pdc )
    {
        m_pdc->SetPen(pen);
        m_pdc->SetBrush(brush);
        m_pdc->DrawPolygon( n, points, xoffset, yoffset );
    }
#ifdef ocpnUSE_GL
    else
    {
        glPushAttrib( GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT | GL_LINE_BIT | GL_HINT_BIT | GL_POLYGON_BIT ); //Save state

        SetGLAttrs( true );

        if( ConfigureBrush(brush) )
        {
            glBegin( GL_POLYGON );
            for( int i = 0; i < n; i++ )
                glVertex2i( points[i].x + xoffset, points[i].y + yoffset );
            glEnd();
        }

        if( ConfigurePen(pen) )
        {
            glBegin( GL_LINE_LOOP );
            for( int i = 0; i < n; i++ )
                glVertex2i( points[i].x + xoffset, points[i].y + yoffset );
            glEnd();
        }
        glPopAttrib();
    }
#endif    
}
