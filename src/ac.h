/******************************************************************************
 *
 * Project:  OpenCPN
 * Purpose:  ActiveCaptain Plugin
 * Author:   Pavel Kalian
 *
 ***************************************************************************
 *   Copyright (C) 2014 by Pavel Kalian                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA.         *
 ***************************************************************************
 */

#ifndef _AC_H_
#define _AC_H_

#include <wx/wxprec.h>

#ifndef  WX_PRECOMP
  #include <wx/wx.h>
#endif //precompiled headers

#include <wx/math.h>

#include "pugixml.hpp"

#include <map>

/** \brief Type of the ActiveCaptain marker
 */
typedef enum AcMarkerType
{
    AC_MARINA,
    AC_ANCHORAGE,
    AC_LOCAL_KNOWLEDGE,
    AC_HAZARD
} AcMarkerType;

/** \brief Type of the ActiveCaptain Local Knowledge
 */
typedef enum AcLkType
{
    AC_LK_UNKNOWN,
    AC_LK_BRIDGE,
    AC_LK_LOCK,
    AC_LK_DAM,
    AC_LK_FERRY,
    AC_LK_INLET,
    AC_LK_BOAT_RAMP,
    AC_LK_MARINE_STORE,
    AC_LK_TOWING,
    AC_LK_SHOPPING,
    AC_LK_AIRPORT
} AcLkType;

/** \brief ActiveCaptain Marker
 *         
 */
class AcMarker
{
public:
    AcMarker();
    AcMarker(const pugi::xml_node node);
    AcMarkerType GetType() { return m_type; }
    wxString GetName() { return m_name; }
    long GetId() { return m_id; }
    void SetId(long id) { m_id = id; }
    double GetLat() { return m_lat; }
    double GetLon() { return m_lon; }
    
    int GetRatingSum() { return m_rating_sum; } //Marina+Anchorage only
    int GetRatingCount() { return m_rating_count; } //Marina+Anchorage only
    int GetRatingStars() { return m_rating_count > 0 ? wxRound( (float)m_rating_sum / (float)m_rating_count ) : 0; }
    AcLkType GetLkType() { return m_lkType; } //Local knowledge only
    wxString GetPhone() { return m_phone; } //Marina only
    int GetCommentCount() { return m_comment_count; } //Hazard only
    unsigned int GetBadges() { return m_badges; }
    
    wxColor GetColor();
    
    void SetGeneral(const wxString& text) { m_txt_general = text; }
    void SetNavigation(const wxString& text) { m_txt_navigation = text; }
    void SetDockage(const wxString& text) { m_txt_dockage = text; }
    void SetFuel(const wxString& text) { m_txt_fuel = text; }
    void SetServices(const wxString& text) { m_txt_services = text; }
    wxString GetGeneral() { return m_txt_general; }
    wxString GetNavigation() { return m_txt_navigation; }
    wxString GetDockage() { return m_txt_dockage; }
    wxString GetFuel() { return m_txt_fuel; }
    wxString GetServices() { return m_txt_services; }
    
private:
    AcMarkerType m_type;
    AcLkType m_lkType;
    wxString m_name;
    long m_id;
    double m_lat, m_lon;
    int m_rating_sum;
    int m_rating_count;
    wxString m_phone;
    int m_comment_count;
    unsigned int m_badges;
    
    wxString m_txt_general;
    wxString m_txt_navigation;
    wxString m_txt_dockage;
    wxString m_txt_fuel;
    wxString m_txt_services;
};


class AcResponse : public pugi::xml_document
{
public:
    AcResponse(const char * response_text);

    wxString GetResult() { return m_Result; }
    wxString GetNextUrl() { return m_NextUrl; }
    wxString GetErrorReason() { return m_ErrorReason; }
    bool IsOk() { return m_bOK; }

protected:
    wxString m_Result;
    
private:
    wxString m_NextUrl;
    wxString m_ErrorReason;
    bool m_bOK;
};

class AcLogin : public AcResponse
{
public:
    AcLogin(const char * response_text);
    wxString GetAPIKey() { return m_APIKey; }
    
    bool Evaluate();

private:
    wxString m_APIKey;
};

class AcMarkerList : public AcResponse
{
public:
    AcMarkerList(const char * response_text);
    std::map<long, AcMarker> GetMarkers() { return m_markers; }
    AcMarker GetMarker(long id) { return m_markers[id]; }
private:
    std::map<long, AcMarker> m_markers;
};

class AcMarkerDetail : public AcResponse
{
public:
    AcMarkerDetail(const char * response_text, AcMarker * marker);
};

#endif
