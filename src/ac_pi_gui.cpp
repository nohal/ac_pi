///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  6 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "ac_pi_gui.h"

///////////////////////////////////////////////////////////////////////////

AcSettingsDialog::AcSettingsDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizerMain;
	bSizerMain = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizerServer;
	sbSizerServer = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _("Server settings") ), wxVERTICAL );
	
	wxFlexGridSizer* fgSizerSettings;
	fgSizerSettings = new wxFlexGridSizer( 0, 2, 0, 0 );
	fgSizerSettings->AddGrowableCol( 1 );
	fgSizerSettings->SetFlexibleDirection( wxBOTH );
	fgSizerSettings->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_stServerUrl = new wxStaticText( this, wxID_ANY, _("Server URL"), wxDefaultPosition, wxDefaultSize, 0 );
	m_stServerUrl->Wrap( -1 );
	fgSizerSettings->Add( m_stServerUrl, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_tServerUrl = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizerSettings->Add( m_tServerUrl, 0, wxALL|wxEXPAND, 5 );
	
	m_stUsername = new wxStaticText( this, wxID_ANY, _("Username"), wxDefaultPosition, wxDefaultSize, 0 );
	m_stUsername->Wrap( -1 );
	fgSizerSettings->Add( m_stUsername, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_tUsername = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizerSettings->Add( m_tUsername, 0, wxALL|wxEXPAND, 5 );
	
	m_stPassword = new wxStaticText( this, wxID_ANY, _("Password"), wxDefaultPosition, wxDefaultSize, 0 );
	m_stPassword->Wrap( -1 );
	fgSizerSettings->Add( m_stPassword, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_tPassword = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD );
	fgSizerSettings->Add( m_tPassword, 0, wxALL|wxEXPAND, 5 );
	
	
	sbSizerServer->Add( fgSizerSettings, 0, wxEXPAND, 5 );
	
	
	bSizerMain->Add( sbSizerServer, 0, wxALL|wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizerDisplay;
	sbSizerDisplay = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _("Display settings") ), wxVERTICAL );
	
	wxStaticBoxSizer* sbSizerObjects;
	sbSizerObjects = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _("Objects to display") ), wxVERTICAL );
	
	wxGridSizer* gSizerShowTypes;
	gSizerShowTypes = new wxGridSizer( 0, 2, 0, 0 );
	
	m_cbMarinas = new wxCheckBox( this, wxID_ANY, _("Marinas"), wxDefaultPosition, wxDefaultSize, 0 );
	m_cbMarinas->SetBackgroundColour( wxColour( 221, 90, 74 ) );
	
	gSizerShowTypes->Add( m_cbMarinas, 0, wxALL|wxEXPAND, 5 );
	
	m_cbHazards = new wxCheckBox( this, wxID_ANY, _("Hazards"), wxDefaultPosition, wxDefaultSize, 0 );
	m_cbHazards->SetBackgroundColour( wxColour( 244, 174, 51 ) );
	
	gSizerShowTypes->Add( m_cbHazards, 0, wxALL|wxEXPAND, 5 );
	
	m_cbAnchorages = new wxCheckBox( this, wxID_ANY, _("Anchorages"), wxDefaultPosition, wxDefaultSize, 0 );
	m_cbAnchorages->SetBackgroundColour( wxColour( 110, 182, 77 ) );
	
	gSizerShowTypes->Add( m_cbAnchorages, 0, wxALL|wxEXPAND, 5 );
	
	m_cbLocalKnowledge = new wxCheckBox( this, wxID_ANY, _("Local knowledge"), wxDefaultPosition, wxDefaultSize, 0 );
	m_cbLocalKnowledge->SetBackgroundColour( wxColour( 30, 104, 240 ) );
	
	gSizerShowTypes->Add( m_cbLocalKnowledge, 0, wxALL|wxEXPAND, 5 );
	
	
	sbSizerObjects->Add( gSizerShowTypes, 0, wxEXPAND, 5 );
	
	
	sbSizerDisplay->Add( sbSizerObjects, 0, wxALL|wxEXPAND, 5 );
	
	
	bSizerMain->Add( sbSizerDisplay, 1, wxALL|wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizerStats;
	sbSizerStats = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _("Session Statistics") ), wxVERTICAL );
	
	wxFlexGridSizer* fgSizerStats;
	fgSizerStats = new wxFlexGridSizer( 0, 4, 0, 0 );
	fgSizerStats->AddGrowableCol( 1 );
	fgSizerStats->AddGrowableCol( 3 );
	fgSizerStats->SetFlexibleDirection( wxBOTH );
	fgSizerStats->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_stQueryNrLbl = new wxStaticText( this, wxID_ANY, _("Nr. of server queries"), wxDefaultPosition, wxDefaultSize, 0 );
	m_stQueryNrLbl->Wrap( -1 );
	fgSizerStats->Add( m_stQueryNrLbl, 0, wxALL, 5 );
	
	m_stQueryNr = new wxStaticText( this, wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	m_stQueryNr->Wrap( -1 );
	fgSizerStats->Add( m_stQueryNr, 0, wxALL, 5 );
	
	m_stDownloadSizeLbl = new wxStaticText( this, wxID_ANY, _("Data downloaded"), wxDefaultPosition, wxDefaultSize, 0 );
	m_stDownloadSizeLbl->Wrap( -1 );
	fgSizerStats->Add( m_stDownloadSizeLbl, 0, wxALL, 5 );
	
	m_stDownloadSize = new wxStaticText( this, wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	m_stDownloadSize->Wrap( -1 );
	fgSizerStats->Add( m_stDownloadSize, 0, wxALL, 5 );
	
	
	sbSizerStats->Add( fgSizerStats, 1, wxEXPAND, 5 );
	
	
	bSizerMain->Add( sbSizerStats, 1, wxALL|wxEXPAND, 5 );
	
	m_sdbButtons = new wxStdDialogButtonSizer();
	m_sdbButtonsOK = new wxButton( this, wxID_OK );
	m_sdbButtons->AddButton( m_sdbButtonsOK );
	m_sdbButtonsCancel = new wxButton( this, wxID_CANCEL );
	m_sdbButtons->AddButton( m_sdbButtonsCancel );
	m_sdbButtons->Realize();
	
	bSizerMain->Add( m_sdbButtons, 0, wxALL|wxEXPAND, 5 );
	
	
	this->SetSizer( bSizerMain );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_sdbButtonsCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AcSettingsDialog::OnCancel ), NULL, this );
	m_sdbButtonsOK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AcSettingsDialog::OnOk ), NULL, this );
}

AcSettingsDialog::~AcSettingsDialog()
{
	// Disconnect Events
	m_sdbButtonsCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AcSettingsDialog::OnCancel ), NULL, this );
	m_sdbButtonsOK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AcSettingsDialog::OnOk ), NULL, this );
	
}

AcMarkerDetailFrame::AcMarkerDetailFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizerMain;
	bSizerMain = new wxBoxSizer( wxVERTICAL );
	
	m_panelHeader = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerHeader;
	bSizerHeader = new wxBoxSizer( wxVERTICAL );
	
	m_stName = new wxStaticText( m_panelHeader, wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	m_stName->Wrap( -1 );
	m_stName->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 92, false, wxEmptyString ) );
	
	bSizerHeader->Add( m_stName, 0, wxALL, 5 );
	
	m_stLocation = new wxStaticText( m_panelHeader, wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	m_stLocation->Wrap( -1 );
	bSizerHeader->Add( m_stLocation, 0, wxALL, 5 );
	
	
	m_panelHeader->SetSizer( bSizerHeader );
	m_panelHeader->Layout();
	bSizerHeader->Fit( m_panelHeader );
	bSizerMain->Add( m_panelHeader, 0, wxEXPAND, 5 );
	
	m_ntbMarkerDetail = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_panelGeneral = new wxPanel( m_ntbMarkerDetail, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerGeneral;
	bSizerGeneral = new wxBoxSizer( wxVERTICAL );
	
	m_htmlGeneral = new wxHtmlWindow( m_panelGeneral, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO );
	bSizerGeneral->Add( m_htmlGeneral, 1, wxEXPAND, 5 );
	
	
	m_panelGeneral->SetSizer( bSizerGeneral );
	m_panelGeneral->Layout();
	bSizerGeneral->Fit( m_panelGeneral );
	m_ntbMarkerDetail->AddPage( m_panelGeneral, _("General"), true );
	m_panelNavigation = new wxPanel( m_ntbMarkerDetail, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerNavigation;
	bSizerNavigation = new wxBoxSizer( wxVERTICAL );
	
	m_htmlNavigation = new wxHtmlWindow( m_panelNavigation, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO );
	bSizerNavigation->Add( m_htmlNavigation, 1, wxEXPAND, 5 );
	
	
	m_panelNavigation->SetSizer( bSizerNavigation );
	m_panelNavigation->Layout();
	bSizerNavigation->Fit( m_panelNavigation );
	m_ntbMarkerDetail->AddPage( m_panelNavigation, _("Navigation"), false );
	m_panelDockage = new wxPanel( m_ntbMarkerDetail, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerDockage;
	bSizerDockage = new wxBoxSizer( wxVERTICAL );
	
	m_htmlDockage = new wxHtmlWindow( m_panelDockage, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO );
	bSizerDockage->Add( m_htmlDockage, 1, wxEXPAND, 5 );
	
	
	m_panelDockage->SetSizer( bSizerDockage );
	m_panelDockage->Layout();
	bSizerDockage->Fit( m_panelDockage );
	m_ntbMarkerDetail->AddPage( m_panelDockage, _("Dockage"), false );
	m_panelFuel = new wxPanel( m_ntbMarkerDetail, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerFuel;
	bSizerFuel = new wxBoxSizer( wxVERTICAL );
	
	m_htmlFuel = new wxHtmlWindow( m_panelFuel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO );
	bSizerFuel->Add( m_htmlFuel, 1, wxEXPAND, 5 );
	
	
	m_panelFuel->SetSizer( bSizerFuel );
	m_panelFuel->Layout();
	bSizerFuel->Fit( m_panelFuel );
	m_ntbMarkerDetail->AddPage( m_panelFuel, _("Fuel"), false );
	m_panelServices = new wxPanel( m_ntbMarkerDetail, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerServices;
	bSizerServices = new wxBoxSizer( wxVERTICAL );
	
	m_htmlServices = new wxHtmlWindow( m_panelServices, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO );
	bSizerServices->Add( m_htmlServices, 1, wxEXPAND, 5 );
	
	
	m_panelServices->SetSizer( bSizerServices );
	m_panelServices->Layout();
	bSizerServices->Fit( m_panelServices );
	m_ntbMarkerDetail->AddPage( m_panelServices, _("Services"), false );
	m_panelRatings = new wxPanel( m_ntbMarkerDetail, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerRatings;
	bSizerRatings = new wxBoxSizer( wxVERTICAL );
	
	m_htmlRatings = new wxHtmlWindow( m_panelRatings, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO );
	bSizerRatings->Add( m_htmlRatings, 1, wxEXPAND, 5 );
	
	
	m_panelRatings->SetSizer( bSizerRatings );
	m_panelRatings->Layout();
	bSizerRatings->Fit( m_panelRatings );
	m_ntbMarkerDetail->AddPage( m_panelRatings, _("Ratings"), false );
	
	bSizerMain->Add( m_ntbMarkerDetail, 1, wxEXPAND | wxALL, 5 );
	
	m_sdbSizerOk = new wxStdDialogButtonSizer();
	m_sdbSizerOkOK = new wxButton( this, wxID_OK );
	m_sdbSizerOk->AddButton( m_sdbSizerOkOK );
	m_sdbSizerOk->Realize();
	
	bSizerMain->Add( m_sdbSizerOk, 0, wxALL|wxEXPAND, 5 );
	
	
	this->SetSizer( bSizerMain );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_sdbSizerOkOK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AcMarkerDetailFrame::OnOk ), NULL, this );
}

AcMarkerDetailFrame::~AcMarkerDetailFrame()
{
	// Disconnect Events
	m_sdbSizerOkOK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( AcMarkerDetailFrame::OnOk ), NULL, this );
	
}
