///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  6 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __AC_PI_GUI_H__
#define __AC_PI_GUI_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/checkbox.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/panel.h>
#include <wx/html/htmlwin.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/notebook.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class AcSettingsDialog
///////////////////////////////////////////////////////////////////////////////
class AcSettingsDialog : public wxDialog 
{
	private:
	
	protected:
		wxStaticText* m_stServerUrl;
		wxTextCtrl* m_tServerUrl;
		wxStaticText* m_stUsername;
		wxTextCtrl* m_tUsername;
		wxStaticText* m_stPassword;
		wxTextCtrl* m_tPassword;
		wxCheckBox* m_cbMarinas;
		wxCheckBox* m_cbHazards;
		wxCheckBox* m_cbAnchorages;
		wxCheckBox* m_cbLocalKnowledge;
		wxStaticText* m_stQueryNrLbl;
		wxStaticText* m_stQueryNr;
		wxStaticText* m_stDownloadSizeLbl;
		wxStaticText* m_stDownloadSize;
		wxStdDialogButtonSizer* m_sdbButtons;
		wxButton* m_sdbButtonsOK;
		wxButton* m_sdbButtonsCancel;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnCancel( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnOk( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		AcSettingsDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("ActiveCaptain Preferences"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 550,450 ), long style = wxDEFAULT_DIALOG_STYLE|wxSTAY_ON_TOP ); 
		~AcSettingsDialog();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class AcMarkerDetailFrame
///////////////////////////////////////////////////////////////////////////////
class AcMarkerDetailFrame : public wxFrame 
{
	private:
	
	protected:
		wxPanel* m_panelHeader;
		wxStaticText* m_stName;
		wxStaticText* m_stLocation;
		wxNotebook* m_ntbMarkerDetail;
		wxPanel* m_panelGeneral;
		wxHtmlWindow* m_htmlGeneral;
		wxPanel* m_panelNavigation;
		wxHtmlWindow* m_htmlNavigation;
		wxPanel* m_panelDockage;
		wxHtmlWindow* m_htmlDockage;
		wxPanel* m_panelFuel;
		wxHtmlWindow* m_htmlFuel;
		wxPanel* m_panelServices;
		wxHtmlWindow* m_htmlServices;
		wxPanel* m_panelRatings;
		wxHtmlWindow* m_htmlRatings;
		wxStdDialogButtonSizer* m_sdbSizerOk;
		wxButton* m_sdbSizerOkOK;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnOk( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		AcMarkerDetailFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Marker detail"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 550,450 ), long style = wxCAPTION|wxMAXIMIZE_BOX|wxRESIZE_BORDER|wxSTAY_ON_TOP|wxTAB_TRAVERSAL );
		
		~AcMarkerDetailFrame();
	
};

#endif //__AC_PI_GUI_H__
