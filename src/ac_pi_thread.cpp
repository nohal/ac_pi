/******************************************************************************
 *
 * Project:  OpenCPN
 * Purpose:  ActiveCaptain Plugin
 * Author:   Pavel Kalian
 *
 ***************************************************************************
 *   Copyright (C) 2014 by Pavel Kalian                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA.         *
 ***************************************************************************
 */

#include "ac_pi.h"
#include "ac_pi_thread.h"



IMPLEMENT_DYNAMIC_CLASS( AcEvent, wxEvent )
DEFINE_EVENT_TYPE( myEVT_MYEVENT )

AcEvent::AcEvent()
{
SetEventType( myEVT_MYEVENT );
}

AcEvent::~AcEvent() {}

wxEvent* AcEvent::Clone() const
{
    AcEvent *newevent = new AcEvent(*this);
    return newevent;
}

/** \brief Worker method of the AC server communication thread
 *
 * \return void*
 *
 */   
void *AcThread::Entry()
{
    wxMilliSleep(500); //Give everything some time to settle down before we try to do our work
    bool can_continue = false;
    m_pHandler->SetThreadRunning(true);
    while (!TestDestroy())
    {
        if( !m_bIsWorking )
        {
            m_bIsWorking = true;
            if( m_bNewQuery && m_pHandler->ShowMarkers() )
            {
                if( !m_pHandler->IsLoggedIn() )
                {
                    can_continue = m_pHandler->LogIn();
                }
                if( can_continue )
                {
                    if( m_Marker.GetId() > 0 )
                    {
                        bool res = m_pHandler->QueryDetail(&m_Marker);
                        AcEvent evt;
                        evt.marker = m_Marker;
                        m_pHandler->AddPendingEvent(evt);
                        m_Marker.SetId(0);
                    }
                    else
                        m_pHandler->QueryArea( m_QueryLat1, m_QueryLon1, m_QueryLat2, m_QueryLon2 );
                }
                m_bNewQuery = false;
            }
            m_bIsWorking = false;
        }
        wxMilliSleep(250);
        //wxQueueEvent(m_pHandler, new wxThreadEvent(wxEVT_COMMAND_ACTHREAD_UPDATE));
    }
    // signal the event handler that this thread is going to be destroyed
    // NOTE: here we assume that using the m_pHandler pointer is safe,
    // (in this case this is assured by the MyFrame destructor)
    //    wxQueueEvent(m_pHandler, new wxThreadEvent(wxEVT_COMMAND_DBTHREAD_COMPLETED));
    //return (wxThread::ExitCode)0; // success

    return 0;
}

/** \brief AC server communication worker thread constructor
 *
 * \param handler ac_pi* Pointer to the plug-in
 *
 */   
AcThread::AcThread(ac_pi * handler) : wxThread()
{
    Create();
    m_pHandler = handler;
    m_bIsWorking = false;
    m_bNewQuery = false;
    m_QueryLat1 = 0.0;
    m_QueryLon1 = 0.0;
    m_QueryLat2 = 0.0;
    m_QueryLon2 = 0.0;
}

/** \brief AC server communication worker thread destructor
 */
AcThread::~AcThread()
{
    wxCriticalSectionLocker enter(m_pHandler->m_pThreadCS);
    m_pHandler->m_pThread = NULL;
    m_pHandler->SetThreadRunning(false);
}

/** \brief Entry point to invoke the retrieval of marker list from the AC server on the background
 *
 * \param lat1 double
 * \param lon1 double
 * \param lat2 double
 * \param lon2 double
 * \return void
 *
 */       
void AcThread::GetData(double lat1, double lon1, double lat2, double lon2)
{
    double dist;
    DistanceBearingMercator_Plugin( lat1, lon1, lat2, lon2, NULL, &dist );
    if( dist > 100. )
    {
        m_pHandler->BlameActiveCaptain(_T("zoom"));
        return; //Do nothing if the area spans more than 100 miles, AC would not return anything anyway in "crowded places"...
    }
    m_bNewQuery = true;
    m_QueryLat1 = lat1;
    m_QueryLon1 = lon1;
    m_QueryLat2 = lat2;
    m_QueryLon2 = lon2;
}

void AcThread::GetDetail(AcMarker* marker)
{
    m_bNewQuery = true;
    m_Marker = *marker;
}
