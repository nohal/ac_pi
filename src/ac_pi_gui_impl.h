/******************************************************************************
 *
 * Project:  OpenCPN
 * Purpose:  ActiveCaptain Plugin
 * Author:   Pavel Kalian
 *
 ***************************************************************************
 *   Copyright (C) 2014 by Pavel Kalian                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA.         *
 ***************************************************************************
 */

#ifndef _ACPI_GUI_IMPL_H_
#define _ACPI_GUI_IMPL_H_

#include <wx/wxprec.h>

#ifndef  WX_PRECOMP
  #include <wx/wx.h>
#endif //precompiled headers

#include "ac_pi_gui.h"
#include "ac_pi.h"

class AcSettingsDialogImpl : public AcSettingsDialog
{
    friend class ac_pi;
public:
    AcSettingsDialogImpl( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("ActiveCaptain Settings"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 550, 450 ), long style = wxDEFAULT_DIALOG_STYLE | wxSTAY_ON_TOP ) : AcSettingsDialog(parent, id, title, pos, size, style) {}
protected:
    void OnOk(wxCommandEvent& event);
private:
    bool ValidateUrl(const wxString url);
    bool ValidateEmail(const wxString Address);
};

class AcMarkerDetailFrameImpl : public AcMarkerDetailFrame
{
public:
    AcMarkerDetailFrameImpl( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Marker detail"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 550, 450 ), long style = wxCAPTION | wxMAXIMIZE_BOX | wxRESIZE_BORDER | wxTAB_TRAVERSAL | wxSTAY_ON_TOP ) : AcMarkerDetailFrame(parent, id, title, pos, size, style) {}
    void SetMarker( AcMarker *marker );
    void SetWaitingForData();
    void OnOk( wxCommandEvent& event ) { Hide(); event.Skip(); }
};

#endif
