/******************************************************************************
 *
 * Project:  OpenCPN
 * Purpose:  ActiveCaptain Plugin
 * Author:   Pavel Kalian
 *
 ***************************************************************************
 *   Copyright (C) 2014 by Pavel Kalian                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA.         *
 ***************************************************************************
 */

 
#include <wx/wxprec.h>

#ifndef  WX_PRECOMP
  #include <wx/wx.h>
#endif //precompiled headers

#include "wx/curl/http.h"

#include "ocpn_plugin.h"
 
#ifndef _ACPIUTIL_H_
#define _ACPIUTIL_H_

bool PointInLLBox( PlugIn_ViewPort *vp, double x, double y );
int NextPow2(int size);

class myCurlHTTP : public wxCurlHTTP
{
public:
    void SetCurlHandleToDefaults(const wxString& relativeURL);
};

#endif
