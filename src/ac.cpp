/******************************************************************************
 *
 * Project:  OpenCPN
 * Purpose:  ActiveCaptain Plugin
 * Author:   Pavel Kalian
 *
 ***************************************************************************
 *   Copyright (C) 2014 by Pavel Kalian                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA.         *
 ***************************************************************************
 */

#include "ac.h"
#include <sstream>

AcResponse::AcResponse(const char * response_text) : pugi::xml_document()
{
    m_Result = wxEmptyString;
    m_ErrorReason = wxEmptyString;
    m_NextUrl = wxEmptyString;
    m_bOK = false;
    
    pugi::xml_parse_result r = load(response_text);
    
    if( !r )
    {
        m_Result = _T("err");
        m_ErrorReason = wxString::FromUTF8( r.description() );
        return;
    }
    
    pugi::xml_node objects = this->child("ActiveCaptain");
    
    if( !objects )
    {
        m_Result = _T("err");
        m_ErrorReason = _("ActiveCaptain root node does not exist in the returned XML.");
        return;
    }
    
    for (pugi::xml_node object = objects.first_child(); object; object = object.next_sibling())
    {
        if( !strcmp(object.name(), "result") )
        {
            m_Result = wxString::FromUTF8( object.first_child().value() );
            if( m_Result != _T("err") )
                m_bOK = true;
        }
        else if( !strcmp(object.name(), "reason") )
        {
            m_ErrorReason = wxString::FromUTF8( object.first_child().value() );
        }
        else if( !strcmp(object.name(), "url") )
        {
            m_NextUrl = wxString::FromUTF8( object.first_child().value() );
        }
    }
}

bool AcLogin::Evaluate()
{
    if( m_Result == _T("OK") )
        return true;
    return false;
}

AcLogin::AcLogin(const char * response_text) : AcResponse(response_text)
{
    if( !IsOk() )
        return;
        
    pugi::xml_node objects = this->child("ActiveCaptain");
    
    for (pugi::xml_node object = objects.first_child(); object; object = object.next_sibling())
    {
        if( !strcmp(object.name(), "uc") )
        {
            m_APIKey = wxString::FromUTF8( object.first_child().value() );
            break;
        }
    }
}

AcMarkerList::AcMarkerList(const char * response_text) : AcResponse(response_text)
{
    m_markers.clear();
    
    if( !IsOk() )
        return;
    
    pugi::xml_node objects = this->child("ActiveCaptain");
    
    for (pugi::xml_node object = objects.first_child(); object; object = object.next_sibling())
    {
        if( !strcmp(object.name(), "marker") )
        {
            AcMarker m(object);
            m_markers[m.GetId()] = m;
        }
    }
}

AcMarkerDetail::AcMarkerDetail(const char * response_text, AcMarker * marker) : AcResponse(response_text)
{
    pugi::xml_node objects = this->child("ActiveCaptain");
    std::ostringstream buffer;
    
    for (pugi::xml_node object = objects.first_child(); object; object = object.next_sibling())
    {
        if( !strcmp(object.name(), "marinaDetails") )
        {
            for (pugi::xml_node child = object.first_child(); child; child = child.next_sibling())
            {
                child.first_child().print(buffer);
                if( !strcmp(child.name(), "general") )
                    marker->SetGeneral( wxString::FromUTF8( buffer.str().c_str() ) );                    
                else if( !strcmp(child.name(), "navigation") )
                    marker->SetNavigation( wxString::FromUTF8( buffer.str().c_str() ) );
                else if( !strcmp(child.name(), "dockage") )
                    marker->SetDockage( wxString::FromUTF8( buffer.str().c_str() ) );
                else if( !strcmp(child.name(), "fuel") )
                    marker->SetFuel( wxString::FromUTF8( buffer.str().c_str() ) );
                else if( !strcmp(child.name(), "services") )
                    marker->SetServices( wxString::FromUTF8( buffer.str().c_str() ) );
                buffer.str("");
                buffer.clear();
            }
        }
        else if ( !strcmp(object.name(), "anchorageDetails") || !strcmp(object.name(), "LKDetails") || !strcmp(object.name(), "hazardDetails") )
        {
            object.first_child().print(buffer);
            marker->SetGeneral( wxString::FromUTF8( buffer.str().c_str() ) );
            marker->SetNavigation( wxEmptyString );
            marker->SetDockage( wxEmptyString );
            marker->SetFuel( wxEmptyString );
            marker->SetServices( wxEmptyString );
        }
    }
}

AcMarker::AcMarker()
{
    m_lkType = AC_LK_UNKNOWN;
    m_name = wxEmptyString;
    m_id = 0;
    m_lat = 0.0;
    m_lon = 0.0;
    m_rating_sum = 0;
    m_rating_count = 0;
    m_phone = wxEmptyString;
    m_comment_count = 0;
    m_badges = 0;
    
    m_txt_general = wxEmptyString;
    m_txt_navigation = wxEmptyString;
    m_txt_dockage = wxEmptyString;
    m_txt_fuel = wxEmptyString;
    m_txt_services = wxEmptyString;
    return;
}

AcMarker::AcMarker(const pugi::xml_node node)
{
    m_lkType = AC_LK_UNKNOWN;
    m_name = wxEmptyString;
    m_id = 0;
    m_lat = 0.0;
    m_lon = 0.0;
    m_rating_sum = 0;
    m_rating_count = 0;
    m_phone = wxEmptyString;
    m_comment_count = 0;
    m_badges = 0;
    
    m_txt_general = wxEmptyString;
    m_txt_navigation = wxEmptyString;
    m_txt_dockage = wxEmptyString;
    m_txt_fuel = wxEmptyString;
    m_txt_services = wxEmptyString;
    
    for (pugi::xml_node object = node.first_child(); object; object = object.next_sibling())
    {
        if( !strcmp(object.name(), "type") )
        {
            if( !strcmp(object.first_child().value(), "M") )
                m_type = AC_MARINA;
            else if( !strcmp(object.first_child().value(), "A") )
                m_type = AC_ANCHORAGE;
            else if( !strcmp(object.first_child().value(), "L") )
                m_type = AC_LOCAL_KNOWLEDGE;
            else if( !strcmp(object.first_child().value(), "H") )
                m_type = AC_HAZARD;
        }
        else if( !strcmp(object.name(), "lk") )
        {
            if( !strcmp(object.first_child().value(), "Bridge") )
                m_lkType = AC_LK_BRIDGE;
            else if( !strcmp(object.first_child().value(), "Lock") )
                m_lkType = AC_LK_LOCK;
            else if( !strcmp(object.first_child().value(), "Dam") )
                m_lkType = AC_LK_DAM;
            else if( !strcmp(object.first_child().value(), "Ferry") )
                m_lkType = AC_LK_FERRY;
            else if( !strcmp(object.first_child().value(), "Inlet") )
                m_lkType = AC_LK_INLET;
            else if( !strcmp(object.first_child().value(), "Boat ramp") )
                m_lkType = AC_LK_BOAT_RAMP;
            else if( !strcmp(object.first_child().value(), "Marine store") )
                m_lkType = AC_LK_MARINE_STORE;
            else if( !strcmp(object.first_child().value(), "Towing") )
                m_lkType = AC_LK_TOWING;
            else if( !strcmp(object.first_child().value(), "Shopping") )
                m_lkType = AC_LK_SHOPPING;
            else if( !strcmp(object.first_child().value(), "Airport") )
                m_lkType = AC_LK_AIRPORT;
            else
                m_lkType = AC_LK_UNKNOWN;
        }
        else if( !strcmp(object.name(), "name") )
        {
            m_name = wxString::FromUTF8( object.first_child().value() );
        }
        else if( !strcmp(object.name(), "ID") )
        {
            m_id = atoi( object.first_child().value() );
        }
        else if( !strcmp(object.name(), "lat") )
        {
            m_lat = atof( object.first_child().value() );
        }
        else if( !strcmp(object.name(), "lon") )
        {
            m_lon = atof( object.first_child().value() );
        }
        else if( !strcmp(object.name(), "ratingSum") )
        {
            m_rating_sum = atoi( object.first_child().value() );
        }
        else if( !strcmp(object.name(), "ratingCount") )
        {
            m_rating_count = atoi( object.first_child().value() );
        }
        else if( !strcmp(object.name(), "commentCount") )
        {
            m_comment_count = atoi( object.first_child().value() );
        }
        else if( !strcmp(object.name(), "phone") )
        {
            m_phone = wxString::FromUTF8( object.first_child().value() );
        }
        else if( !strcmp(object.name(), "badge") )
        {
            m_badges = atoi( object.first_child().value() );
        } 
    }
}

wxColor AcMarker::GetColor()
{
    if( m_type == AC_MARINA )
        return wxColour(0x4a5add);
    else if( m_type == AC_ANCHORAGE )
        return wxColour(0x4db66e);
    else if( m_type == AC_LOCAL_KNOWLEDGE )
        return wxColour(0xf0681e);
    else if( m_type == AC_HAZARD )
        return wxColour(0x33aef4);
    return wxColour(0xaaaaaa); //Fallback default in case AC will introduce new marker types
}
