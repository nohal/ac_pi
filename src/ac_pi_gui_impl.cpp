/******************************************************************************
 *
 * Project:  OpenCPN
 * Purpose:  ActiveCaptain Plugin
 * Author:   Pavel Kalian
 *
 ***************************************************************************
 *   Copyright (C) 2014 by Pavel Kalian                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA.         *
 ***************************************************************************
 */


#include "ac_pi_gui_impl.h"
#include <wx/regex.h>

bool AcSettingsDialogImpl::ValidateUrl(const wxString Url)
{
    wxRegEx re(_T("^https?\\://[a-zA-Z0-9\\./_-]*$") ); //TODO: wxRegEx sucks a bit, this RE is way too naive
    return re.Matches(Url);
}

bool AcSettingsDialogImpl::ValidateEmail(const wxString Address)
{
    wxRegEx re(_T("^[a-zA-Z0-9\\._-]{1,}@[a-zA-Z0-9\\._-]*\\.[a-zA-Z]{2,4}$") ); //TODO: wxRegEx sucks a bit, this RE is way too naive
    return re.Matches(Address);
}

void AcSettingsDialogImpl::OnOk(wxCommandEvent& event)
{
    bool valid = true;
    wxString msg = wxEmptyString;
    if( !ValidateUrl(m_tServerUrl->GetValue()) )
    {
        
        msg.Append( _("The server URL seems invalid.") ).Append( _T("\n") );
        valid = false;
    }
    if( !ValidateEmail(m_tUsername->GetValue()) )
    {
        msg.Append( _("The username seems invalid, it has to be an e-mail address.") ).Append( _T("\n") );
        valid = false;
    }
    if( m_tPassword->GetValue().Len() < 3 )
    {
        msg.Append( _("The password seems invalid, it has to be at least 3 characters long.") ).Append( _T("\n") );
        valid = false;
    }
    if( !(m_cbMarinas->GetValue() || m_cbAnchorages->GetValue() || m_cbHazards->GetValue() || m_cbLocalKnowledge->GetValue()) )
    {
        msg.Append( _("You have not selected any POI type to be displayed, don't be surprised that you won't see anything...") );
        //valid = false;
    }
    
    if( msg != wxEmptyString )
        wxMessageBox(msg);
    
    event.Skip(valid);
}

void AcMarkerDetailFrameImpl::SetWaitingForData()
{
    m_htmlGeneral->SetPage(_("<html><body><b>Waiting for data from AC server...</b></body></html>"));
    m_panelGeneral->Show();
}


void AcMarkerDetailFrameImpl::SetMarker( AcMarker* marker )
{
    if( !marker )
        return;
    m_panelHeader->SetBackgroundColour( marker->GetColor() );
    m_stName->SetLabel( marker->GetName() );
    m_stLocation->SetLabel( wxString::Format( _("%.4f, %.4f"), marker->GetLat(), marker->GetLon() ) );
    m_htmlGeneral->SetPage( marker->GetGeneral() );
    m_htmlNavigation->SetPage( marker->GetNavigation() );
    m_htmlDockage->SetPage( marker->GetDockage() );
    m_htmlFuel->SetPage( marker->GetFuel() );
    m_htmlServices->SetPage( marker->GetServices() );
    
    if( marker->GetGeneral() == wxEmptyString )
        m_panelGeneral->Hide();
    else
        m_panelGeneral->Show();
    if( marker->GetNavigation() == wxEmptyString )
        m_panelNavigation->Hide();
    else
        m_panelNavigation->Show();
    if( marker->GetDockage() == wxEmptyString )
        m_panelDockage->Hide();
    else
        m_panelDockage->Show();
    if( marker->GetFuel() == wxEmptyString )
        m_panelFuel->Hide();
    else
        m_panelFuel->Show();
    if( marker->GetServices() == wxEmptyString )
        m_panelServices->Hide();
    else
        m_panelServices->Show();
    m_panelRatings->Hide(); //TODO: Implement if the API is a bit reasonable
}
