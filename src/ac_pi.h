/******************************************************************************
 *
 * Project:  OpenCPN
 * Purpose:  ActiveCaptain Plugin
 * Author:   Pavel Kalian
 *
 ***************************************************************************
 *   Copyright (C) 2014 by Pavel Kalian                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA.         *
 ***************************************************************************
 */

#ifndef _ACPI_H_
#define _ACPI_H_

#include <wx/wxprec.h>

#ifndef  WX_PRECOMP
  #include <wx/wx.h>
#endif //precompiled headers

#include <wx/fileconf.h>

#include "md5.h"
#include "wx/curl/http.h"
#include "TexFont.h"

#include "version.h"

#include "ocpn_plugin.h"

#include "icons.h"

#include "ac_pi_util.h"
#include "ac_pi_thread.h"
#include "ac_pi_draw.h"
#include "ac.h"

#define     MY_API_VERSION_MAJOR    1
#define     MY_API_VERSION_MINOR    12

//----------------------------------------------------------------------------------------------------------
//    The PlugIn Class Definition
//----------------------------------------------------------------------------------------------------------

#define AC_TOOL_POSITION    -1          // Request default positioning of toolbar tool

#define LOGIN_URL _T("?c=login&e=%s&p=%s")
#define AREA_URL _T("?c=markers&minx=%f&maxx=%f&miny=%f&maxy=%f&m=%i&a=%i&l=%i&h=%i&uc=%s")
#define DETAIL_URL _T("?c=details_html&id=%lu&uc=%s")

typedef void (wxEvtHandler::*myEventFunction)(AcEvent&);

class AcMarkerDetailFrameImpl;

class ac_pi : public opencpn_plugin_112, public wxEvtHandler
{
public:
    ac_pi(void *ppimgr);

//    The required PlugIn Methods
    int Init(void);
    bool DeInit(void);

    int GetAPIVersionMajor() { return MY_API_VERSION_MAJOR; }
    int GetAPIVersionMinor() { return MY_API_VERSION_MINOR; }
    int GetPlugInVersionMajor() { return PLUGIN_VERSION_MAJOR; }
    int GetPlugInVersionMinor() { return PLUGIN_VERSION_MINOR; }
    wxBitmap *GetPlugInBitmap() { return _img_ac_pi; }
    wxString GetCommonName() { return _T("activecaptain"); }
    wxString GetShortDescription() { return _("ActiveCaptain PlugIn for OpenCPN"); }
    wxString GetLongDescription();

//    The required override PlugIn Methods
    int GetToolbarToolCount ( void ) { return 1; }
    void OnToolbarToolCallback ( int id );
    void SetCursorLatLon(double lat, double lon);
    void SetCurrentViewPort(PlugIn_ViewPort &vp);
    void RenderOverlayBoth(wxDC *dc, PlugIn_ViewPort *vp);
    bool RenderOverlay(wxDC &dc, PlugIn_ViewPort *vp);
    bool RenderGLOverlay(wxGLContext *pcontext, PlugIn_ViewPort *vp);
    void ShowPreferencesDialog( wxWindow* parent );
    bool MouseEventHook( wxMouseEvent &event );

//    Other public methods
    static wxString Rot13(const wxString text);

//    AC Declarations
    void SetThreadRunning(bool state) { m_bThreadRuning = state; }
    bool IsThreadRunning() { return m_bThreadRuning; }
    
    void OnThreadDetailAvailable(AcEvent& event);

protected:
    bool LogIn();
    bool QueryArea(double lat1, double lon1, double lat2, double lon2);
    bool QueryDetail(AcMarker* marker);
    bool IsLoggedIn() { return m_bIsLoggedIn; }
    bool ShowMarkers();
    bool SetMarkerToForm( AcMarker* marker );
    void BlameActiveCaptain(wxString err_reason);
    void UnblameActiveCaptain();
    
    void LogMessage(const wxString& msg);

private:
    PlugIn_ViewPort m_vp;
    AcMarker *m_ActiveMarker;
    wxFileConfig *m_pconfig;
    bool LoadConfig(void);
    bool SaveConfig(void);
    
    bool m_bDebug;
    wxString m_AcMsg;
    wxFont *m_dFont_map;
    TexFont m_TexFontMessage;
    void DrawMessageWindow( wxString msg, int x, int y, wxFont *mfont );
    void DrawGLLine( double x1, double y1, double x2, double y2, double width );
    bool ConfigurePen(wxPen pen);
    bool ConfigureBrush(wxBrush brush);
    void DrawPolygon( int n, wxPoint points[], wxCoord xoffset, wxCoord yoffset, wxPen pen, wxBrush brush );
    static void SetGLAttrs( bool highQuality );
    bool DataAvailable(double lat1, double lon1, double lat2, double lon2);
    void ExtendArea(double lat1, double lon1, double lat2, double lon2, double* newlat1, double* newlon1, double* newlat2, double* newlon2);
    
    wxWindow *m_parent_window;

    int m_leftclick_tool_id;
    
    wxString m_sUsername;
    wxString m_sPassword;
    wxString m_sServerUrl;
    wxString m_sAPIKey;
    bool m_bShow;
    bool m_bShowMarinas;
    bool m_bShowAnchorages;
    bool m_bShowLocalKnowledge;
    bool m_bShowHazards;
    
    wxString m_sNextUrl;
    
    wxString EncodePassword(const wxString pwd) { return wxMD5::GetMD5( ac_pi::Rot13( pwd.Lower() ) ); }
    bool ShouldDisplay(AcMarker marker);

    AcThread *m_pThread;
    wxCriticalSection m_pThreadCS; // protects the m_pThread pointer
    friend class AcThread; // allow it to access our m_pThread
    
    bool m_bThreadRuning;
    
    bool m_bIsLoggedIn;
    bool m_bAccountError;
    
    std::map<long, AcMarker> m_markers;
    std::map<int, AcIconTexture> m_badgeTextures;
    
    wxDC *m_pdc;
    
    void DrawMarker( wxPoint hotspot, AcMarker marker );
    void DrawUnrolledMarker( wxPoint hotspot, AcMarker marker );
    void DrawStar( wxPoint hotspot, wxPen pen, wxBrush brush );
    void DrawExclamation( wxPoint hotspot, wxPen pen, wxBrush brush );
    void DrawStars( wxPoint hotspot, AcMarker marker );
    void DrawBitmap( wxBitmap bmp, int x, int y );
    unsigned int GetIconTexture( const wxBitmap *pbm, int &glw, int &glh );
    void GetTextExtent( wxString msg, wxFont *mfont, int *w, int *h );
    void DrawText( wxString msg, wxPoint hotspot, wxFont *mfont );
    double m_DataLat1;
    double m_DataLon1;
    double m_DataLat2;
    double m_DataLon2;
    
    size_t m_querynr;
    size_t m_querybytes;
    
    bool m_bPanning;
    
    AcMarkerDetailFrameImpl *m_pDetailDlg;
    
    DECLARE_EVENT_TABLE()
};

#endif
