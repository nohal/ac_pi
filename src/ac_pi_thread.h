/******************************************************************************
 *
 * Project:  OpenCPN
 * Purpose:  ActiveCaptain Plugin
 * Author:   Pavel Kalian
 *
 ***************************************************************************
 *   Copyright (C) 2014 by Pavel Kalian                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA.         *
 ***************************************************************************
 */

#ifndef _ACPITHREAD_H_
#define _ACPITHREAD_H_

#include <wx/wxprec.h>

#ifndef  WX_PRECOMP
  #include <wx/wx.h>
#endif //precompiled headers

#include "ac.h"

class ac_pi;

BEGIN_DECLARE_EVENT_TYPES()
	DECLARE_EVENT_TYPE( myEVT_MYEVENT, 1 )
END_DECLARE_EVENT_TYPES()

class AcEvent : public wxEvent
{
public:
    AcEvent();
    ~AcEvent();
    wxEvent *Clone() const;
    AcMarker marker;
    
    DECLARE_DYNAMIC_CLASS(AcEvent)
};

#define EVT_MYEVENT(func)                      \
	DECLARE_EVENT_TABLE_ENTRY( myEVT_MYEVENT,  \
		-1,                                    \
		-1,                                    \
		(wxObjectEventFunction)                \
		(myEventFunction) & func,      \
		(wxObject *) NULL ),

class AcThread : public wxThread
{
public:
    AcThread(ac_pi * handler);
    ~AcThread();
    void *Entry();
    
    bool IsWorking() { return m_bIsWorking; }
    void GetData(double lat, double lon, double lat1, double lon1);
    void GetDetail(AcMarker* marker);
protected:
    ac_pi *m_pHandler;

private:
    bool m_bIsWorking;
    bool m_bNewQuery;
    double m_QueryLat1;
    double m_QueryLon1;
    double m_QueryLat2;
    double m_QueryLon2;
    AcMarker m_Marker;
};

#endif
