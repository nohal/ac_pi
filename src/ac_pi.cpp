/******************************************************************************
 *
 * Project:  OpenCPN
 * Purpose:  ActiveCaptain Plugin
 * Author:   Pavel Kalian
 *
 ***************************************************************************
 *   Copyright (C) 2014 by Pavel Kalian                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA.         *
 ***************************************************************************
 */

#include "ac_pi.h"
#include "ac_pi_gui_impl.h"

// the class factories, used to create and destroy instances of the PlugIn

extern "C" DECL_EXP opencpn_plugin* create_pi(void *ppimgr)
{
    return new ac_pi(ppimgr);
}

extern "C" DECL_EXP void destroy_pi(opencpn_plugin* p)
{
    delete p;
}

//---------------------------------------------------------------------------------------------------------
//
//    ActiveCaptain PlugIn Implementation
//
//---------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------
//
//        PlugIn initialization and de-init
//
//---------------------------------------------------------------------------------------------------------

BEGIN_EVENT_TABLE( ac_pi, wxEvtHandler )
	EVT_MYEVENT( ac_pi::OnThreadDetailAvailable )
END_EVENT_TABLE()

/** @brief The plug-in constructor
 *
 * @param ppimgr void* Pointer to the OpenCPN's plug-in manager
 *
 */   
ac_pi::ac_pi(void *ppimgr)
    : opencpn_plugin_112(ppimgr)
{
    // Create the PlugIn icons
    initialize_images();
}

/** @brief Initialize the plug-in and create all the internal data structures
 *
 * @param void
 * @return int The core events the plug-in is interested in being notified about
 *
 */    
int ac_pi::Init(void)
{
    m_ActiveMarker = NULL;
    m_pdc = NULL;
    m_sNextUrl = wxEmptyString;
    SetThreadRunning(false);
    m_bIsLoggedIn = false;
    m_DataLat1 = 0.0;
    m_DataLon1 = 0.0;
    m_DataLat2 = 0.0;
    m_DataLon2 = 0.0;
    m_bAccountError = false;
    m_querynr = 0;
    m_querybytes = 0;
    m_bPanning = false;
    wxCurlHTTP::Init();
    
    m_dFont_map = new wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL );
    
    AddLocaleCatalog( _T("opencpn-ac_pi") );
    m_pconfig = GetOCPNConfigObject();
    m_parent_window = GetOCPNCanvasWindow();

    m_leftclick_tool_id = InsertPlugInTool( wxEmptyString, _img_ac, _img_ac, wxITEM_CHECK, _("Show/Hide ActiveCaptain Markers"), wxEmptyString, NULL, AC_TOOL_POSITION, 0, this );
    
    LoadConfig();
    
    SetToolbarItemState( m_leftclick_tool_id, m_bShow );

    m_pThread = new AcThread(this);
    wxThreadError err = m_pThread->Run();

    if ( err != wxTHREAD_NO_ERROR )
    {
        delete m_pThread;
        m_pThread = NULL;
    }
    
    m_pDetailDlg = new AcMarkerDetailFrameImpl(m_parent_window);
    
    return (WANTS_OVERLAY_CALLBACK      |
          WANTS_OPENGL_OVERLAY_CALLBACK |
          WANTS_TOOLBAR_CALLBACK        |
          INSTALLS_TOOLBAR_TOOL         |
          WANTS_CURSOR_LATLON           |
          WANTS_PREFERENCES             |
          WANTS_MOUSE_EVENTS            |
          WANTS_ONPAINT_VIEWPORT        |
          WANTS_CONFIG
         );
}

/** @brief Deinitialize the plug-in. In case the plug-in uses threads, they MUST be stopped and disposed here, not in the destructor.
 *
 * @param void
 * @return bool Result of the deinitialization, usually true
 *
 */    
bool ac_pi::DeInit(void)
{
    SaveConfig();
    
    m_pDetailDlg->Close(true);
    delete m_pDetailDlg;
        
    {
        wxCriticalSectionLocker enter(m_pThreadCS);
        if (m_pThread) // does the thread still exist?
        {
            while (m_pThread->IsWorking())
            {
                wxMilliSleep(10);
            }
            if (m_pThread->Delete() != wxTHREAD_NO_ERROR )
                wxLogError(_T("Can't delete the DB thread!"));
        }
    } // exit from the critical section to give the thread
        // the possibility to enter its destructor
        // (which is guarded with m_pThreadCS critical section!)
    while (1)
    {
        { // was the ~MyThread() function executed?
            wxCriticalSectionLocker enter(m_pThreadCS);
            if (!m_pThread)
                break;
        }
        // wait for thread completion
    }
    
    //Last resort check for thread completion, wait if it looks bad
    #define THREAD_WAIT_SECONDS  5
    //  Try to wait a bit to see if all compression threads exit nicely
    wxDateTime now = wxDateTime::Now();
    time_t stall = now.GetTicks();
    time_t end = stall + THREAD_WAIT_SECONDS;
    
    while(IsThreadRunning() && stall < end ){
        wxDateTime later = wxDateTime::Now();
        stall = later.GetTicks();
        
        wxYield();
        wxSleep(1);
        if( !IsThreadRunning() )
            break;
    }
    
    wxCurlHTTP::Shutdown();
    
    return true;
}

/** @brief Return long descriprion of the plug-in
 *
 * @return wxString The description of the plug-in
 *
 */   
wxString ac_pi::GetLongDescription()
{
    return _("ActiveCaptain\n\
This is ActiveCaptain plug-in for OpenCPN.");
}

/** @brief Perform actions when the plug-in toolbar tool icon was clicked
 *
 * @param id int Id of the tool clicked
 * @return 
 *
 */     
void ac_pi::OnToolbarToolCallback ( int id )
{
    m_bShow = !m_bShow;
}

/** \brief Called when the core viewport changes
 *
 * \param vp PlugIn_ViewPort& Core Viewport
 * \return void
 *
 */    
void ac_pi::SetCurrentViewPort(PlugIn_ViewPort &vp)
{
    if( !m_bAccountError && IsThreadRunning() && ShowMarkers() )
    {
        m_vp = vp;
        if( !m_pThread->IsWorking() && !m_bPanning )
            m_pThread->GetData( vp.lat_min, vp.lon_min, vp.lat_max, vp.lon_max );
    }
}


/** \brief Renders the overlay. Depending on the graphical mode used OpenGL or wxDC is used. 
 *
 * \param dc wxDC* Drawing canvas
 * \param vp PlugIn_ViewPort* Core viewport parameters
 * \return void
 *
 */     
void ac_pi::RenderOverlayBoth(wxDC *dc, PlugIn_ViewPort *vp)
{
    if( !ShowMarkers() || m_bPanning )
        return; //If there is nothing to display or the left mouse button is down, which means the user pans the canvas
    m_vp = *vp;
    AcMarker unrolled;
    bool unroll = false;
    wxPoint pl, unpl;
    
    std::map<long, AcMarker>::iterator it;
    for (it = m_markers.begin(); it != m_markers.end(); it++) {
        AcMarker marker = it->second;
        if( ShouldDisplay(marker) && PointInLLBox( vp, marker.GetLon(), marker.GetLat() ) )
        {
            GetCanvasPixLL( vp, &pl, marker.GetLat(), marker.GetLon() );
            if( m_ActiveMarker && marker.GetId() == m_ActiveMarker->GetId() )
            {
                unroll = true;
                unpl = pl;
                unrolled = marker;
            }
            else
                DrawMarker(pl, marker);
        }
    }
    if( unroll )
        DrawUnrolledMarker(unpl, unrolled);
    DrawMessageWindow( m_AcMsg , vp->pix_width, vp->pix_height, m_dFont_map );
}

/** \brief Called by the core when the viewport changes in non-OpenGL mode
 *
 * \param dc wxDC& Drawing canvas
 * \param vp PlugIn_ViewPort* Core viewport parameters
 * \return bool
 *
 */
bool ac_pi::RenderOverlay(wxDC &dc, PlugIn_ViewPort *vp)
{
    m_pdc = &dc;
    RenderOverlayBoth(&dc, vp);
    return true;
}

/** \brief Called by the core when the viewport changes in OpenGL mode
 *
 * \param dc wxDC& Drawing canvas
 * \param vp PlugIn_ViewPort* Core viewport parameters
 * \return bool
 *
 */
bool ac_pi::RenderGLOverlay(wxGLContext *pcontext, PlugIn_ViewPort *vp)
{
    m_pdc = NULL;
    RenderOverlayBoth(NULL, vp);
    return true;
}

/** \brief Checks whether the AC marker should be displayed according to the user settings
 *
 * \param marker AcMarker The AC marker
 * \return bool True if it is configured to be displayed
 *
 */    
bool ac_pi::ShouldDisplay(AcMarker marker)
{
    if( marker.GetType() == AC_MARINA && m_bShowMarinas )
        return true;
    if( marker.GetType() == AC_ANCHORAGE && m_bShowAnchorages )
        return true;
    if( marker.GetType() == AC_LOCAL_KNOWLEDGE && m_bShowLocalKnowledge )
        return true;
    if( marker.GetType() == AC_HAZARD && m_bShowHazards )
        return true;
    return false;
}

/** \brief Core event sent when the mouse cursor location changes
 *
 * \param lat double Latitude
 * \param lon double Longitude
 * \return void
 *
 */     
void ac_pi::SetCursorLatLon(double lat, double lon)
{
    bool refreshneeded = (bool)m_ActiveMarker;
    m_ActiveMarker = NULL;
    wxPoint cur;
    GetCanvasPixLL( &m_vp, &cur, lat, lon );
    std::map<long, AcMarker>::iterator it;
    for (it = m_markers.begin(); it != m_markers.end(); it++)
    {
        AcMarker *marker = &it->second;
        if( ShouldDisplay(*marker) && PointInLLBox( &m_vp, marker->GetLon(), marker->GetLat() ) )
        {
            wxPoint pl;
            GetCanvasPixLL( &m_vp, &pl, marker->GetLat(), marker->GetLon() );
            if (pl.x > cur.x - 10 && pl.x < cur.x + 10 && pl.y > cur.y && pl.y < cur.y + 20)
            {
                m_ActiveMarker = marker;
                RequestRefresh(m_parent_window);
                m_parent_window->SetCursor(wxCURSOR_HAND);
                break;
            }
        }
    }
    if( !m_ActiveMarker || (m_ActiveMarker && refreshneeded && m_ActiveMarker->GetId() == 0) )
    {
        RequestRefresh(m_parent_window);
        m_parent_window->SetCursor(wxCURSOR_DEFAULT);
    }
}

void ac_pi::OnThreadDetailAvailable(AcEvent& event)
{
    AcMarker m = event.marker;
    SetMarkerToForm(&m);
}


/** \brief Sets the Marker to the details form, if open
 *
 * \param marker AcMarker& Marker to show
 * \return bool True if the form was open and it made sense to set the values, false if not
 *
 */    
bool ac_pi::SetMarkerToForm( AcMarker* marker )
{
    if( !m_pDetailDlg || marker->GetId() == 0 )
        return false;
    m_pDetailDlg->SetMarker( marker );
    return true;
}

/** \brief Processes the mouse event
 *
 * \param event wxMouseEvent& The mouse event
 * \return bool True if we handled the event and don't want the core to do anything about it, false otherwise
 *
 */    
bool ac_pi::MouseEventHook( wxMouseEvent &event )
{
    m_bPanning = event.Dragging();
    if( event.LeftUp() )
    {
        m_bPanning = false;
        RequestRefresh(m_parent_window);
    }
    if( (bool)m_ActiveMarker )
    {
        if( event.LeftUp() )
        {
            m_pDetailDlg->SetMarker(m_ActiveMarker);
            m_pDetailDlg->SetWaitingForData();
            m_pDetailDlg->Show();
            m_pThread->GetDetail(m_ActiveMarker);
            return true;
        }
    }
    return false; //The event should be further processed by the core
}

/** \brief Called by core when the Settings dialog is requested
 *
 * \param parent wxWindow* Parent core window
 * \return void
 *
 */    
void ac_pi::ShowPreferencesDialog( wxWindow* parent )
{
    AcSettingsDialogImpl *dialog = new AcSettingsDialogImpl( parent );

    dialog->m_tUsername->SetValue(m_sUsername);
    dialog->m_tPassword->SetValue(m_sPassword);
    dialog->m_tServerUrl->SetValue(m_sServerUrl);
    
    dialog->m_cbMarinas->SetValue(m_bShowMarinas);
    dialog->m_cbAnchorages->SetValue(m_bShowAnchorages);
    dialog->m_cbLocalKnowledge->SetValue(m_bShowLocalKnowledge);
    dialog->m_cbHazards->SetValue(m_bShowHazards);
    
    wxString lbl = wxString::Format(_T("%u"), m_querynr);
    dialog->m_stQueryNr->SetLabel(lbl);
    lbl = wxString::Format(_("%u bytes"), m_querybytes);
    dialog->m_stDownloadSize->SetLabel(lbl);
    
    if(dialog->ShowModal() == wxID_OK)
    {
        if( m_sUsername != dialog->m_tUsername->GetValue() ||
            m_sPassword != dialog->m_tPassword->GetValue() ||
            m_sServerUrl != dialog->m_tServerUrl->GetValue() )
        {
            m_bIsLoggedIn = false; //Log the user out
            m_bAccountError = false; //Hope he fixed the problem
        }
        m_sUsername = dialog->m_tUsername->GetValue();
        m_sPassword = dialog->m_tPassword->GetValue();
        m_sServerUrl = dialog->m_tServerUrl->GetValue();
        
        m_bShowMarinas = dialog->m_cbMarinas->GetValue();
        m_bShowAnchorages = dialog->m_cbAnchorages->GetValue();
        m_bShowLocalKnowledge = dialog->m_cbLocalKnowledge->GetValue();
        m_bShowHazards = dialog->m_cbHazards->GetValue();

        SaveConfig();
    }
    delete dialog;
}

/** \brief Loads the settings of the plug-in from OpenCPN's configuration file
 *
 * \param void
 * \return bool True in case of success, false if the configuration object does not exist
 *
 */    
bool ac_pi::LoadConfig(void)
{
    wxFileConfig *pConf = (wxFileConfig *)m_pconfig;

    if(pConf)
    {
        pConf->SetPath( _T( "/Settings/ActiveCaptain" ) );
        pConf->Read( _T( "Username" ), &m_sUsername, wxEmptyString );
        pConf->Read( _T( "Password" ), &m_sPassword, wxEmptyString );
        m_sPassword = ac_pi::Rot13( m_sPassword );
        pConf->Read( _T( "ServerUrl" ), &m_sServerUrl, _T("http://www.activecaptain.com/API/v1.php") );
        //pConf->Read( _T( "APIKey" ), &m_sAPIKey, wxEmptyString );
        pConf->Read( _T( "Debug" ), &m_bDebug, false );
        
        pConf->Read( _T( "Show" ), &m_bShow, false );
        pConf->Read( _T( "ShowMarinas" ), &m_bShowMarinas, true );
        pConf->Read( _T( "ShowAnchorages" ), &m_bShowAnchorages, true );
        pConf->Read( _T( "ShowLocalKnowledge" ), &m_bShowLocalKnowledge, true );
        pConf->Read( _T( "ShowHazards" ), &m_bShowHazards, true );
        
        return true;
    }
    else
        return false;
}

/** \brief Saves the settings of the plug-in into OpenCPN's configuration file
 *
 * \param void
 * \return bool True in case of success, false if the configuration object does not exist
 *
 */   
bool ac_pi::SaveConfig(void)
{
    wxFileConfig *pConf = (wxFileConfig *)m_pconfig;

    if(pConf)
    {
        pConf->SetPath( _T ( "/Settings/ActiveCaptain" ) );
        pConf->Write( _T ( "Username" ), m_sUsername );
        pConf->Write( _T ( "Password" ), ac_pi::Rot13( m_sPassword ) );
        pConf->Write( _T ( "ServerUrl" ), m_sServerUrl );
        //pConf->Write( _T ( "APIKey" ), m_sAPIKey );
        
        pConf->Write( _T( "Show" ), m_bShow );
        pConf->Write( _T( "ShowMarinas" ), m_bShowMarinas );
        pConf->Write( _T( "ShowAnchorages" ), m_bShowAnchorages );
        pConf->Write( _T( "ShowLocalKnowledge" ), m_bShowLocalKnowledge );
        pConf->Write( _T( "ShowHazards" ), m_bShowHazards );
        return true;
    }
    else
        return false;
}


/** \brief Checks whether the configuration demands any markers to be displayed
 *
 * \return bool True if at least some markers should be considered
 *
 */   
bool ac_pi::ShowMarkers()
{
    return m_bShow & (m_bShowAnchorages | m_bShowHazards | m_bShowLocalKnowledge | m_bShowMarinas);
}

/** @brief Performs ROT13 on supplied string
 *
 * @param text const wxString The text to rotate
 * @return wxString The text with rotated values
 *
 */    
wxString ac_pi::Rot13(const wxString text)
{
    wxChar c, c1;
    wxString res = wxEmptyString;
    for( size_t i = 0; i < text.Len(); i++ )
    {
        c = text.GetChar(i);
        c1 = c;
        if( (c >= 'a' && c <= 'm') || (c >= 'A' && c <= 'M') )
        {
            c1 += 13;
        }
        else if( (c >= 'n' && c <= 'z') || (c >= 'N' && c <= 'Z') )
        {
            c1 -= 13;
        }
        res.Append(c1);
    }
    return res;
}

/** @brief Performs the login operation
 *
 * @return bool Result of the operation, true in case of a success
 *
 */   
bool ac_pi::LogIn()
{
    if( m_bAccountError )
        return false;
    char * response;
    wxString url = wxString::Format( LOGIN_URL, m_sUsername.c_str(), EncodePassword( m_sPassword ).c_str() );
    LogMessage( m_sServerUrl + url );
    m_querynr++;
    myCurlHTTP http;
    BlameActiveCaptain( _T("querying") );
    size_t result = http.Get( response, m_sServerUrl + url );
    if( result )
    {
        m_querybytes += result;
        LogMessage( _T("Login response: ") + wxString::FromUTF8( response) );
        AcLogin r( response );
        result = r.Evaluate();
        if( result )
        {
            m_sAPIKey = r.GetAPIKey();
            m_sNextUrl = r.GetNextUrl();
            LogMessage( _T("API key: ") + m_sAPIKey );
            LogMessage( _T("Next URL: ") + m_sNextUrl );
            UnblameActiveCaptain();
        }
        else
        {
            BlameActiveCaptain( r.GetErrorReason() );
        }
    }
    else
    {
        BlameActiveCaptain( _("Network connection error.") );
    }
    delete[] response;
    m_bIsLoggedIn = result;
    RequestRefresh(m_parent_window);
    return result;
}

/** \brief Checks whether the currently loaded data cover the entire newly needed area
 *
 * \param lat1 double Minimum latitude
 * \param lon1 double Minimum longitude
 * \param lat2 double Maximum latitude
 * \param lon2 double Maximum longitude
 * \return bool True in case the data are already available
 *
 */       
bool ac_pi::DataAvailable(double lat1, double lon1, double lat2, double lon2)
{
    if( lat1 > m_DataLat1 && lon1 > m_DataLon1 && lat2 < m_DataLat2 && lon2 < m_DataLon2 )
    {
        LogMessage(wxString::Format(_T("We already have data for %f, %f - %f, %f - cache: %f, %f - %f, %f"), lat1, lon1, lat2, lon2, m_DataLat1, m_DataLon1, m_DataLat2, m_DataLon2));
        UnblameActiveCaptain();
        return true;
    }
    return false;
}

/** \brief Extends the area for the query so that we have some more data - should save us some queries especially while autofollowing the boat
 *
 * \param lat1 double Minimum latitude
 * \param lon1 double Minimum longitude
 * \param lat2 double Maximum latitude
 * \param lon2 double Maximum longitude
 * \param newlat1 double& Extended min latitude
 * \param newlon1 double& Extended min longitude
 * \param newlat2 double& Extended max latitude
 * \param newlon2 double& Extended max longitude
 *
 */      
void ac_pi::ExtendArea(double lat1, double lon1, double lat2, double lon2, double* newlat1, double* newlon1, double* newlat2, double* newlon2)
{
    double extension_ratio = .5; //50% bigger in every direction
    double latdiff = lat2 - lat1;
    double londiff = lon2 - lon1;
    *newlat1 = lat1 - latdiff * extension_ratio;
    *newlon1 = lon1 - londiff * extension_ratio;
    *newlat2 = lat2 + latdiff * extension_ratio;
    *newlon2 = lon2 + londiff * extension_ratio;
    
    if( *newlat1 < -80.0 )
        *newlat1 = -80.0;
    if( *newlat2 > 80.0 )
        *newlat2 = 80.0;
    if( *newlon1 > 180.0 )
        *newlon1 -= 360.0;
    else if( *newlon1 < -180.0 )
        *newlon1 += 360.0;
    if( *newlon2 > 180.0 )
        *newlon2 -= 360.0;
    else if( *newlon2 < -180.0 )
        *newlon2 += 360.0;
}

/** @brief Performs the query for markers
 *
 * @return bool Result of the operation, true in case of a success
 *
 */   
bool ac_pi::QueryArea(double lat1, double lon1, double lat2, double lon2)
{
    if( m_bAccountError || !m_bIsLoggedIn || DataAvailable(lat1, lon1, lat2, lon2) )
        return true; //We already have the data or can't query anyway because of login problems
    char * response;
    //Make the area a bit bigger so that we don't have to requery immediately
    double l_lat1;
    double l_lon1;
    double l_lat2;
    double l_lon2;
    ExtendArea(lat1, lon1, lat2, lon2, &l_lat1, &l_lon1, &l_lat2, &l_lon2);
    wxString url = wxString::Format( AREA_URL, l_lon1, l_lon2, l_lat1, l_lat2, m_bShowMarinas, m_bShowAnchorages, m_bShowLocalKnowledge, m_bShowHazards, m_sAPIKey.c_str() );
    LogMessage( m_sServerUrl + url );
    m_querynr++;
    myCurlHTTP http;
    BlameActiveCaptain( _T("querying") );
    size_t result = http.Get( response, m_sServerUrl + url );
    if( result )
    {
        if( m_bDebug )
            LogMessage( wxString::FromUTF8(response) );
        m_querybytes += result;
        AcMarkerList r( response );
        if( r.IsOk() )
        {
            
            LogMessage(wxString::Format(_T("querying for markers in %f, %f - %f, %f"), l_lat1, l_lon1, l_lat2, l_lon2));
            m_markers = r.GetMarkers();
            m_sNextUrl = r.GetNextUrl();
            m_DataLat1 = l_lat1;
            m_DataLon1 = l_lon1;
            m_DataLat2 = l_lat2;
            m_DataLon2 = l_lon2;
            UnblameActiveCaptain();
            RequestRefresh(m_parent_window);
        }
        else
        {
            BlameActiveCaptain( r.GetErrorReason() );
        }
    }
    else
    {
        BlameActiveCaptain( _("Network connection error.") );
    }
    delete[] response;
    RequestRefresh(m_parent_window);
    return result;
}

bool ac_pi::QueryDetail(AcMarker* marker)
{
    if( m_bAccountError || !m_bIsLoggedIn )
        return true;
    char * response;

    wxString url = wxString::Format( DETAIL_URL, marker->GetId(), m_sAPIKey.c_str() );
    LogMessage( m_sServerUrl + url );
    m_querynr++;
    myCurlHTTP http;
    BlameActiveCaptain( _T("querying") );
    size_t result = http.Get( response, m_sServerUrl + url );
    if( result )
    {
        m_querybytes += result;
        AcMarkerDetail r( response, marker );
        if( r.IsOk() )
        {
            m_sNextUrl = r.GetNextUrl();
            UnblameActiveCaptain();
        }
        else
        {
            BlameActiveCaptain( r.GetErrorReason() );
        }
    }
    else
    {
        BlameActiveCaptain( _("Network connection error.") );
    }
    delete[] response;
    RequestRefresh(m_parent_window);
    return result;
}

/** \brief Logs one message
 *
 * \param msg const wxString& Message to be logged
 * \return void
 *
 */    
void ac_pi::LogMessage(const wxString& msg)
{
    if( m_bDebug )
        wxLogMessage(_T("AC: ") + msg);
}

/** \brief Removes the message shown to the user in the corner of the viewport
 *
 * \return void
 *
 */   
void ac_pi::UnblameActiveCaptain()
{
    m_AcMsg = wxEmptyString;
}

/** @brief Sets a message in the corner of the screen to an error returned by AC or a status message.
 *
 * @param err_reason wxString The reason of the error occurred
 * @return void
 *
 */    
void ac_pi::BlameActiveCaptain(wxString err_reason)
{
    wxString err_msg;
    
    if( err_reason == _T("querying") )
        err_msg = _("Query in progress...");
    else if( err_reason == _T("zoom") )
        err_msg = _("Not querying for new markers, zoom further in.");
    else if( err_reason == _T("params") )
        err_msg = _("There are parameter errors in the request.");
    else if( err_reason == _T("query") )
        err_msg = _("There was an internal query error that was logged. Contact ActiveCaptain for assistance with examining the logs.");
    else if( err_reason == _T("acct") )
    {
        //TODO: acct is also returned if AC does not like "uc" in the data queries
        //Probably it will be easier to change the err_reason in the data queries...
        err_msg = _("The email parameter does not exist as an ActiveCaptain account.");
        m_bAccountError = true;
    }
    else if( err_reason == _T("pwd") )
    {
        err_msg = _("The password is not correct for this account.");
        m_bAccountError = true;
    }
    else if( err_reason == _T("validation") )
        err_msg = _("The user's account has been registered but has not yet been validated.");
    else if( err_reason == _T("notypes") )
        err_msg = _("All of the marker types were turned off so nothing can be returned.");
    else if( err_reason == _T("size") )
        err_msg = _("Server says the area includes too many markers to return, zoom in.");
    else 
        err_msg = wxString::Format( _("Unknown error: %s"), err_reason.c_str() );
    LogMessage( err_reason + _T(" ") + err_msg );
    m_AcMsg = _T("AC: ") + err_msg;
}
